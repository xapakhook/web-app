const {User} = require('../models')
const jwt = require('jsonwebtoken')
const config = require('../config/config')

function jwtSignUser (user) {
  const ONE_WEEK = 60 * 60 * 24 * 7
  return jwt.sign(user, config.authentication.jwtSecret, {
    expiresIn: ONE_WEEK
  })
}

module.exports = {
  async register (req, res) {
    try {
      const user = await User.create(req.body)
      console.log('user was created')
      const UserJson = user.toJSON()
      res.send({
        user: UserJson,
        token: jwtSignUser(UserJson)
      })
    } catch (err) {
      console.log('user not created')
      res.status(400).send({
        error: 'This email is already in use.'
      })
    }
  },
  async login (req, res) {
    try {
      const {email, password} = req.body
      const user = await User.findOne({
        where: {
          email: email
        }
      })
      if (!user) {
        console.log('user not found!')
        res.status(403).send({
          error: 'The login information was incorrect'
        })
      }

      const isPasswordValid = await user.comparePassword(password)
      console.log(isPasswordValid)
      if (!isPasswordValid) {
        console.log('password is wrong')
        res.status(403).send({
          error: 'The login information was incorrect'
        })
      }
      const UserJson = user.toJSON()
      res.send({
        user: UserJson,
        token: jwtSignUser(UserJson)
      })
      console.log('login success!')
    } catch (err) {
      res.status(500).send({
        error: 'The login error occured'
      })
    }
  }
}
