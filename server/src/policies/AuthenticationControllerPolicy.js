const Joi = require('joi')

module.exports = {
  register (req, res, next) {
    const schema = {
      email: Joi.string().email(),
      password: Joi.string().regex(
        new RegExp('^[a-zA-Z0-9]{8,32}$')
      )
    }

    const {error, value} = Joi.validate(req.body, schema)

    if (error) {
      switch (error.details[0].context.key) {
        case 'email':
          res.status(400).send({
            error: 'Email: You must provide a valid email adress'
          })
          break
        case 'password':
          res.status(400).send({
            error: 'Password: a-z or A-Z or 0-9 characters only with 8 up to 32 characters in lenght'
          })
          break
        default:
          res.status(400).send({
            error: 'Invalid registration information'
          })
      }
    } else {
      console.log(value)
      next()
    }
  }
}
